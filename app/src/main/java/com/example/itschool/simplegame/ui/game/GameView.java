package com.example.itschool.simplegame.ui.game;

/**
 * Created by IT SCHOOL on 01.02.2017.
 */
public interface GameView {
    void updateExpression(String exp);
    void updateTimer(String time);
}
