package com.example.itschool.simplegame.ui.game;

import android.content.Context;

import com.example.itschool.simplegame.utils.RandomExpManager;
import com.example.itschool.simplegame.utils.model.Expression;
import com.example.itschool.simplegame.utils.model.Result;

/**
 * Created by IT SCHOOL on 01.02.2017.
 */
public class GamePresenter {

    private Context mContext;
    private GameView mView;

    private Result mResult;
    private Expression mExpression;

    public GamePresenter (Context context, GameView view) {
        this.mContext = context;
        this.mView = view;
    }

    public void startGame() {
        Expression e = RandomExpManager.generateExpression(RandomExpManager.DIFF_EASY);
        mView.updateExpression(e.expression);
    }

    public void onPositiveClick() {
        Expression e = RandomExpManager.generateExpression(RandomExpManager.DIFF_EASY);
        mView.updateExpression(e.expression);
    }

    public void onNegativeClick() {
        Expression e = RandomExpManager.generateExpression(RandomExpManager.DIFF_EASY);
        mView.updateExpression(e.expression);
    }

}
