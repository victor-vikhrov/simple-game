package com.example.itschool.simplegame.ui.menu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.itschool.simplegame.R;
import com.example.itschool.simplegame.ui.game.GameActivity;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, GameActivity.class);
        intent.putExtra(GameActivity.INTENT_KEY_DIFFICULT, 1);
        startActivity(intent);
    }

}
