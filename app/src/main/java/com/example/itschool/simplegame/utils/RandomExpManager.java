package com.example.itschool.simplegame.utils;

import com.example.itschool.simplegame.utils.model.Expression;

/**
 * Created by IT SCHOOL on 01.02.2017.
 */
public class RandomExpManager {

    public final static int DIFF_EASY = 1;
    public final static int DIFF_MID = 2;
    public final static int DIFF_HARD = 3;

    public static Expression generateExpression(int difficult) {
        switch (difficult) {
            case DIFF_EASY:
                return generateEasy();
            case DIFF_MID:
                return generateMid();
            default: DIFF_HARD:
                return generateHard();
        }
    }

    private static Expression generateHard() {
        return new Expression("", false);
    }

    private static Expression generateMid() {
        return new Expression("", false);
    }

    private static Expression generateEasy() {
        int n1 = (int) (Math.random() * 10);
        int n2 = (int) (Math.random() * 10);

        int res = ((int) (Math.random() * 2));
        boolean sign = (res == 1);
        boolean correct = false;
        String s = n1 + "";
        if (sign) {
            s += " > ";
            if (n1 > n2) correct = true;
        } else  {
            s += " < ";
            if (n1 < n2) correct = true;
        }
        s += n2;

        Expression exp = new Expression(s, correct);


        return exp;
    }

}
