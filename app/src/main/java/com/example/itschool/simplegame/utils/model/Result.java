package com.example.itschool.simplegame.utils.model;

/**
 * Created by IT SCHOOL on 01.02.2017.
 */
public class Result {

    int score;
    int difficult;
    String playerName;

    Result (String name, int difficult, int score) {
        this.playerName = name;
        this.difficult = difficult;
        this.score = score;
    }

}
