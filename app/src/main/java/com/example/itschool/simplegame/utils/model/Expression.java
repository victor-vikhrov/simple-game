package com.example.itschool.simplegame.utils.model;

/**
 * Created by IT SCHOOL on 01.02.2017.
 */
public class Expression {
    public String expression;
    public boolean isRight;

    public Expression(String expression, boolean isRight) {
        this.expression = expression;
        this.isRight = isRight;
    }

}
