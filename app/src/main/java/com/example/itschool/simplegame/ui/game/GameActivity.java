package com.example.itschool.simplegame.ui.game;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.itschool.simplegame.R;

import org.w3c.dom.Text;

public class GameActivity extends AppCompatActivity implements View.OnClickListener, GameView {

    public static String INTENT_KEY_DIFFICULT = "difficult";

    private TextView mTVTimer;
    private TextView mTVExpression;
    private Button mBPositive, mBNegative;

    private GamePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        mTVTimer = (TextView) findViewById(R.id.tv_timer);
        mTVExpression = (TextView) findViewById(R.id.tv_expression);
        mBNegative = (Button) findViewById(R.id.btn_no);
        mBPositive = (Button) findViewById(R.id.btn_yes);

        mBPositive.setOnClickListener(this);
        mBNegative.setOnClickListener(this);

        mPresenter = new GamePresenter(this, this);
        mPresenter.startGame();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_no:
                mPresenter.onNegativeClick();
                break;
            case R.id.btn_yes:
                mPresenter.onPositiveClick();
                break;
        }
    }

    @Override
    public void updateExpression(String exp) {
        mTVExpression.setText(exp);
    }

    @Override
    public void updateTimer(String time) {
        mTVTimer.setText(time);
    }
}
